# To do App

## Requirements

- Not use any library or framework
- Use typescript or babel
- Apply MVP
- Use Sass

### Design

![design ](https://gitlab.com/eber.quenta.bootcamp/resources/-/raw/master/DesignOfTodoApp.gif)

```puml
@startuml To do App
interface IMainView
interface IMainPresenter

abstract class BaseView {
  #_container: HTMLElement
  +render()
  #append():void
  #createElement():HTMLElement
}

class MainPresenter{
  -_view: MainView
}
class MainView {
  -_presenter: IMainPresenter
}

MainPresenter ..> IMainPresenter
IMainView -o MainPresenter
MainView --> BaseView
MainView ..> IMainView
MainView -- IMainPresenter

@enduml
```
import { BaseView, DOCUMENT } from "../../Common/BaseView";
import { Task } from "../../Models/Entities/Task";
import TaskService, { TASK_SERVICE } from "../../Models/Service/TaskService";
import { TaskView } from "../Task/TaskItem/TaskView";
import { TaskFormView } from "../Task/TaskForm/TaskFormView";
import MainPresenter, { IMainPresenter } from "./MainPresenter";
import { inject, injectable } from "inversify";
export const MAIN_VIEW = Symbol('MAIN_VIEW')

export interface IMainView {
  setTasks(tasks: Task[]): void;
}
@injectable()
export default class MainView extends BaseView implements IMainView {
  private _presenter: IMainPresenter;
  constructor(@inject(DOCUMENT) _document: Document, @inject(TASK_SERVICE) taskService: TaskService) {
    const classStyle: string = 'main-view'
    super(_document, classStyle)
    this._presenter = new MainPresenter(this, taskService)
    this._presenter.onLoadAddTaskList() // add everytask in de db
  }
  setTasks(tasks: Task[]): void {
    this._container.innerHTML = ''
    tasks.map(task => new TaskView(this._document, task, this._presenter.onDeleteTask, this._presenter.onCheckTask)).forEach(t => this.append(t.render()))
    this.setForm()
  }
  setForm() {
    const form = new TaskFormView(this._document, this._presenter.onAddTask)
    this.append(form.render())
  }
}
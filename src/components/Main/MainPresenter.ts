import { Task } from "../../Models/Entities/Task"

import { IMainView } from "./MainView"
import TaskService from "../../Models/Service/TaskService"

export interface IMainPresenter {
  onLoadAddTaskList(): any
  onDeleteTask(id: string): void
  onAddTask(description: string): any
  onCheckTask(id: string): void
}
export default class MainPresenter implements IMainPresenter {
  private _view: IMainView
  private _taskService: TaskService
  constructor(view: IMainView, taskService: TaskService) {
    this._view = view
    this._taskService = taskService
  }
  onDeleteTask = (id: string) => {
    this._taskService.deleteTask(id)
    const fakeTasks = this._taskService.getAllTasks()
    this._view.setTasks(fakeTasks)
  }
  onCheckTask = (id: string) => {
    this._taskService.patchTask(id)
    const fakeTasks = this._taskService.getAllTasks()
    this._view.setTasks(fakeTasks)
  }

  onLoadAddTaskList = () => {
    const fakeTasks = this._taskService.getAllTasks()
    this._view.setTasks(fakeTasks)
  }
  onAddTask = (description: string) => {
    this._taskService.post(description)
    const fakeTasks = this._taskService.getAllTasks()
    this._view.setTasks(fakeTasks)
  }
}
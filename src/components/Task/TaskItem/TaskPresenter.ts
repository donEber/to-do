import { ITaskView, TaskView } from './TaskView'
export interface ITaskPresenter {
  onTaskDelete(id: string):any
}
export class TaskPresenter implements ITaskPresenter {
  constructor(private _view: ITaskView) {

  }
  onTaskDelete(id: string) {
    console.log({ id });
  }
}
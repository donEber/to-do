import { BaseView } from "../../../Common/BaseView";
import { Task } from "../../../Models/Entities/Task";
import { ITaskPresenter, TaskPresenter } from "./TaskPresenter";

export interface ITaskView {

}
export class TaskView extends BaseView implements ITaskView {
  private _presenter: ITaskPresenter;
  private _taskData: Task
  constructor(_document: Document, taskData: Task, onDel: (id: string) => void, onCheck: (id: string) => void) {
    super(_document, 'task')
    this._taskData = taskData;
    this._presenter = new TaskPresenter(this)


    // 2 Render html one task
    const p = this.createElement('div', this._taskData.doneState?'task-content-done':'task-content');
    p.textContent = this._taskData.description
    const del = this.createElement('button', 'task-delete');
    del.textContent = '×'
    del.addEventListener('click', () => onDel(this._taskData.id))
    const check = this.createElement('input', 'task-check');
    check.type = "checkbox"
    check.checked = this._taskData.doneState
    check.addEventListener('click', () => onCheck(this._taskData.id))
    this.append(p)
    this.append(del)
    this.append(check)

  }

}
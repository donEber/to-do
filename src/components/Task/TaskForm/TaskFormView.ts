import { BaseView } from "../../../Common/BaseView";

export interface ITaskFormView {

}
export class TaskFormView extends BaseView implements ITaskFormView {
  constructor(_document: Document, onAddTask: (description: string) => void) {
    super(_document, '');
    // PUT THE FORM F
    const input = this.createElement('input', 'send-box--input')
    input.placeholder = 'Add to do task'
    const submitBtn = this.createElement('button', 'send-box--button')
    submitBtn.textContent = '+'
    submitBtn.addEventListener('click', () => input.value && onAddTask(input.value))

    const formDiv = this.createElement('div', 'send-box')
    formDiv.appendChild(input)
    formDiv.appendChild(submitBtn)
    this.append(formDiv)
  }
}
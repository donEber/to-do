import { injectable } from "inversify"
export const DOCUMENT = Symbol('DOCUMENT')
@injectable()
export class BaseView {
    protected _container: HTMLElement

    constructor(protected _document: Document, classes: string, mainTag: string = 'div') {
        this._container = this.createElement(mainTag, classes)
    }
    render(): HTMLElement {
        return this._container
    }

    protected append(element: HTMLElement) {
        this._container.append(element)
    }

    protected createElement(tag: string, classes: string): HTMLElement {
        const element = this._document.createElement(tag)
        const cls = classes.split(' ')
        if (classes)
            element.classList.add(...cls)
        return element
    }
}
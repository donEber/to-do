import 'reflect-metadata'
import MainView, { IMainView, MAIN_VIEW } from "./components/Main/MainView";
import TaskService, { TASK_SERVICE } from "./Models/Service/TaskService";
import "./Styles/app.scss";
import { Container } from 'inversify'
import HttpClient, { HTTP_CLIENT, IHttpClient } from './Models/Utils/HttpClient';
import { DOCUMENT } from './Common/BaseView';
import { LOCAL_STORAGE } from './Models/db';
const container = new Container()
container.bind<IHttpClient>(HTTP_CLIENT).to(HttpClient)
container.bind<TaskService>(TASK_SERVICE).to(TaskService)
container.bind<IMainView>(MAIN_VIEW).to(MainView)
container.bind<Document>(DOCUMENT).toConstantValue(document)
container.bind<Object>(LOCAL_STORAGE).toConstantValue(localStorage)


// const mainView = new MainView(document, new TaskService(new HttpClient(localStorage)));
const mainView = container.get<IMainView>(MAIN_VIEW) as MainView
const app: any = document.querySelector("#app");
app.appendChild(mainView.render())
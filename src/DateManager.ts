
const date: any = document.querySelector("#date");
const dayNumber: number = new Date().getDate()
date.innerHTML = String(dayNumber);

const str: any = document.querySelector('#string')
str.innerHTML = dayNumber == 1 ? 'st' : dayNumber == 2 ? 'nd' : dayNumber == 3 ? 'rd' : 'th'

const month: any = document.querySelector("#month");
month.innerHTML = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Decembe',][new Date().getMonth()]
export interface Task {
  id: string;
  description: string;
  doneState: false;
}
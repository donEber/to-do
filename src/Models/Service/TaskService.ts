
import { inject, injectable, } from 'inversify';
import { Task } from '../Entities/Task';

import { HTTP_CLIENT, IHttpClient } from "../Utils/HttpClient";
export const TASK_SERVICE = Symbol('TASK_SERVICE')
//  (TASK_SERVICE)
@injectable()
export default class TaskService {
    constructor(@inject(HTTP_CLIENT) private _http: IHttpClient) {

    }
    post = (taskDescription: string): Task => {
        return this._http.post('/tasks', taskDescription)
    }
    patchTask(id: string) {
        return this._http.patch('/tasks/id', id)
    }
    getAllTasks(): Task[] {
        return this._http.get('/tasks');
    }
    deleteTask = (id: string) => {
        return this._http.delete('/tasks', id)
    }
    addTask(description: string) {
        return this._http.post('/tasks', description)
    }
}
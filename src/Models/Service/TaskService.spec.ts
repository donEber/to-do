import "reflect-metadata"
import "inversify"
import { Task } from "../Entities/Task"
import { IHttpClient } from "../Utils/HttpClient"
import TaskService from "./TaskService"

describe('TaskService', () => {
  it('Should return tasks (+)', () => {
    const httpStub = <IHttpClient>{ get: (s: any): Array<Task> => { return [] } }
    const Iservice = new TaskService(httpStub)
    const tasks = Iservice.getAllTasks()
    expect(tasks.length).toBeGreaterThanOrEqual(0)
  })
})
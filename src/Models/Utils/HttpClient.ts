import { inject, injectable } from 'inversify';
import { LOCAL_STORAGE } from '../db';
import { Task } from '../Entities/Task';
export const HTTP_CLIENT = Symbol('HTTP_CLIENT')
export interface IHttpClient {
  get(resource: string): Task[]
  post(resource: string, taskDescription: string): Task
  patch(resource: string, id: string): void
  delete(resource: string, id: string): Task
}
@injectable()
export default class HttpClient implements IHttpClient {
  constructor(@inject(LOCAL_STORAGE) private _localStorage: any) {

  }
  post(resource: string = "/tasks", taskDescription: string): Task {
    const db = this._localStorage.info == null ? [] : JSON.parse(this._localStorage.info)
    const newTask: Task = { id: String(new Date().getTime()), description: taskDescription, doneState: false }
    db.push(newTask)
    this._localStorage.info = JSON.stringify(db)
    return newTask
  }
  delete(resource: string = "/tasks", id: string): Task {
    console.log('Deleting', { id });
    const db = this._localStorage.info == null ? [] : JSON.parse(this._localStorage.info)
    const taskDeleted: Task = db.find((task: Task) => task.id == id);
    const newDB = db.filter((task: Task) => task.id != id)
    this._localStorage.info = JSON.stringify(newDB)
    return taskDeleted
  }
  patch(resource: string = "/tasks", id: string) {
    const db = this._localStorage.info == null ? [] : JSON.parse(this._localStorage.info)
    const taskPatched: Task = db.find((task: Task) => task.id == id);
    const newDB = db.map((task: any) => {
      if (task.id == id)
        task.doneState = !task.doneState
      return task
    })
    this._localStorage.info = JSON.stringify(newDB)
  }
  get(resource: string = "/tasks"): Task[] {
    return this._localStorage.info == null ? [] : JSON.parse(this._localStorage.info)
  }
}